import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class FormFirefox {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.gecko.driver","C:\\geckodriver.exe");
	    
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://google.com");
		driver.get("http://localhost:8080/usuarios/new");
		
		
		// Captura o Insere users
		WebElement nome = driver.findElement(By.name("usuario.nome"));
		WebElement email = driver.findElement(By.name("usuario.email"));
		
		
		// Insere user
		nome.sendKeys("Ronaldo Luiz de Albuquerque");
		email.sendKeys("ronaldo2009@terra.com.br");
		
		
		
		// Insere via id=btnSalvar do HTML
		WebElement btnSalvar = driver.findElement(By.id("btnSalvar"));
		btnSalvar.click();
		// Insere via submit fun��o do Selenium
		//nome.submit();
		//email.submit();
		
		public static void aguardaAteQueValueMude(final WebElement element)	{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition() {
		public Boolean apply(WebDriver driver) {
		String value = element.getAttribute(�value�);
		if(!value.equals(��)) {
		return true;
		}
		return false;
		}
		});
		}
		driver.quit();
	}
}
