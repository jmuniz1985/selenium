import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class AutoTest {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.gecko.driver","C:\\geckodriver.exe");
	    
		WebDriver driverFF = new FirefoxDriver();
		
		driverFF.get("http://google.com");
		
		WebElement campoTextoFF = driverFF.findElement(By.name("q"));

		campoTextoFF.sendKeys("pesquisa FireFox via selenium");
	
		campoTextoFF.submit();


		
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
	    
		WebDriver driverCH = new ChromeDriver();
		
		driverCH.get("http:google.com");
		
		WebElement campoTextoCH = driverCH.findElement(By.name("q"));

		campoTextoCH.sendKeys("pesquisa CHROME via selenium");
	
		campoTextoCH.submit();
		
	}
}




		