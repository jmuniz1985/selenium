import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class AtomatestingWebForm {
	
	public static void main(String[] args) {
		//Acessa a page
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
	    WebDriver driver  = new ChromeDriver();
		driver.get("localhost:8080/usuarios/new");
		
		
		// Captura o Insere users
		WebElement nome = driver.findElement(By.name("usuario.nome"));
		WebElement email = driver.findElement(By.name("usuario.email"));
		
		
		// Insere user
		nome.sendKeys("Ronaldo Luiz de Albuquerque");
		email.sendKeys("ronaldo2009@terra.com.br");
		
		
		
		// Insere via id=btnSalvar do HTML
		WebElement btnSalvar = driver.findElement(By.id("btnSalvar"));
		btnSalvar.click();
		
		
		// Insere via submit fun��o do Selenium
		//nome.submit();
		//email.submit();
			
		driver.quit();
	}

}
